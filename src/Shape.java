public abstract class Shape {
    protected String shapeName;

    public Shape(String newName) {
        shapeName = newName;
    }

    public abstract double area();

    public String toString() {
        String result = "Shape name: " + shapeName;
        return result;
    }
}
